import { AppProps } from "next/app";
import Head from "next/head";
import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";
import "../styles/globals.css";
import "../components/loading/loading.css";
import { Provider } from "react-redux";
import store from "../store/index";

const App = ({ Component, pageProps }: AppProps) => {
  return (
    <>
      <Head>
        <title>Rick and Morty Challenge</title>
        <meta
          name="description"
          content="Website for searching information about Rick and Morty's universe"
        />
      </Head>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </>
  );
};

export default App;
