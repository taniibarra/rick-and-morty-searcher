import { v4 as uuidv4 } from "uuid";
import Layout from "../components/layout/layout.component";
import CardCharacters from "../components/cards/card-character.component";
import CardLocEp from "../components/cards/card-loc-ep.component";
import Paginator from "../components/paginator/paginator.component";
import Loading from "../components/loading/loading.component";
import NotFound from "../components/not-found/not-found.component";
import Error from "../components/error/error.component";
import { connect } from "react-redux";

const mapStateToProps = (state: any) => {
  return {
    filter: state.filterReducer.filter,
    searchText: state.searchTextReducer.searchText,
    result: state.resultReducer.result,
    pagination: state.paginationReducer.pagination,
    loading: state.loadingReducer.loading,
  };
};
interface HomeProps {
  result?: object | any;
  pagination?: number;
  loading?: boolean;
  characters?: any;
}

const Home: React.FC<HomeProps> = ({ result, pagination, loading }) => {
  let totalPages = 0;

  if (result.characters) {
    totalPages = result.characters.info.pages;
  } else if (result.locations) {
    totalPages = result.locations.info.pages;
  } else if (result.episodes) {
    totalPages = result.episodes.info.pages;
  }

  return (
    <>
      <Layout>
        <div className="d-flex flex-wrap justify-content-center">
          {loading ? <Loading /> : null}

          {result.notFound ? <NotFound /> : null}
          {result.error ? <Error /> : null}

          {result.characters
            ? result.characters.results.map((data: any) => (
                <CardCharacters
                  name={data.name}
                  img={data.image}
                  key={uuidv4()}
                  modal={data}
                />
              ))
            : null}
          {result.locations
            ? result.locations.results.map((data: any) => (
                <CardLocEp
                  name={data.name}
                  dimension={data.dimension}
                  key={uuidv4()}
                  modal={data}
                />
              ))
            : null}
          {result.episodes
            ? result.episodes.results.map((data: any) => (
                <CardLocEp
                  name={data.name}
                  episode={data.episode}
                  key={uuidv4()}
                  modal={data}
                />
              ))
            : null}
        </div>
        {totalPages > 1 ? (
          <Paginator total={totalPages} actual={pagination} />
        ) : null}
      </Layout>
    </>
  );
};

export default connect(mapStateToProps)(Home);
