import ApolloClient from "apollo-boost";
import { gql } from "apollo-boost";

const client = new ApolloClient({
  uri: "https://rickandmortyapi.com/graphql",
});

export const qry = async (txt: string) => {
  try {
    return await client.query({
      query: gql`query {${txt}}`,
      fetchPolicy: "no-cache",
    });
  } catch (error) {
    throw error;
  }
};
