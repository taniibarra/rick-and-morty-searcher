import { useState } from "react";
import ModalCharacters from "../modals/modal-characters.component";

interface CardCharacterProps {
  name: string;
  img: string;
  modal?: boolean;
}

const CardCharacter: React.FC<CardCharacterProps> = ({ name, img, modal }) => {
  const [open, setOpen] = useState(false);

  const toggle = () => {
    setOpen(!open);
  };

  return (
    <>
      <div
        className="col-12 col-sm-4 col-md-3 col-lg-2 cards m-2 white z-depth-3"
        onClick={toggle}
      >
        <div className="my-2 col-12 text-center">
          <img className="img-fluid" src={img} alt={name} />
        </div>
        <p className="text-center font-weight-bold mb-2">{name}</p>
      </div>
      {open ? <ModalCharacters data={modal} /> : null}
    </>
  );
};

export default CardCharacter;
