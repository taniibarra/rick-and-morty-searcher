import { useState } from "react";
import ModalLocEp from "../modals/modal-loc-ep.component";

interface CardLocEpProps {
  name: string;
  dimension?: any;
  episode?: any;
  modal?: boolean;
}

const CardLocEp: React.FC<CardLocEpProps> = ({
  name,
  dimension,
  episode,
  modal,
}) => {
  const [open, setOpen] = useState(false);

  const toggle = () => {
    setOpen(!open);
  };

  return (
    <>
      <div
        className="col-10 col-sm-5 col-md-3 col-lg-2 cards m-2 white z-depth-3"
        onClick={toggle}
      >
        <p className="text-center font-weight-bold mt-4">{name}</p>
        {dimension ? <p className="text-center mt-4">{dimension}</p> : null}
        {episode ? <p className="text-center mt-4">{episode}</p> : null}
      </div>
      {open ? <ModalLocEp data={modal} /> : null}
    </>
  );
};

export default CardLocEp;
