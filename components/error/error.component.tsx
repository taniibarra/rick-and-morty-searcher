import { MDBContainer, MDBAlert } from "mdbreact";

const Error = () => {
  return (
    <>
      <MDBContainer>
        <MDBAlert color="danger" className="text-center">
          Connection Error
        </MDBAlert>
      </MDBContainer>
    </>
  );
};

export default Error;
