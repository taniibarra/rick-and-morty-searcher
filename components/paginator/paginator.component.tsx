import { MDBPagination, MDBPageItem, MDBPageNav } from "mdbreact";
import updatePagination from "../../store/pagination/pagination.action";
import { connect } from "react-redux";

const Paginator = ({ total, actual, updatePagination }) => {
  return (
    <>
      <MDBPagination className="my-5 justify-content-center" color="dark-grey">
        <MDBPageItem>
          <MDBPageNav
            aria-label="Previous"
            onClick={() => {
              updatePagination(actual - 1);
            }}
          >
            <span aria-hidden="true">Previous</span>
          </MDBPageNav>
        </MDBPageItem>

        {Array.from({ length: total + 1 }, (_, k) =>
          k > 0 ? (
            k === actual ? (
              <MDBPageItem key={k} active>
                <MDBPageNav
                  key={k}
                  onClick={() => {
                    updatePagination(k);
                  }}
                >
                  {k}
                  <span className="sr-only">(current)</span>
                </MDBPageNav>
              </MDBPageItem>
            ) : (
              <MDBPageItem key={k}>
                <MDBPageNav
                  key={k}
                  onClick={() => {
                    updatePagination(k);
                  }}
                >
                  {k}
                </MDBPageNav>
              </MDBPageItem>
            )
          ) : null
        )}

        <MDBPageItem>
          <MDBPageNav
            aria-label="Previous"
            onClick={() => {
              updatePagination(actual + 1);
            }}
          >
            <span aria-hidden="true">Next</span>
          </MDBPageNav>
        </MDBPageItem>
      </MDBPagination>
    </>
  );
};

export default connect(null, { updatePagination })(Paginator);
