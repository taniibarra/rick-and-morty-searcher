import updateFilter from "../../store/filter/filter.action";
import updatePagination from "../../store/pagination/pagination.action";
import updateResult from "../../store/result/result.action";
import { connect } from "react-redux";

interface SidebarProps {
  updateFilter?: any;
  updatePagination?: any;
  updateResult?: any;
  className?: string;
}

const Sidebar: React.FC<SidebarProps> = ({
  updateFilter,
  updatePagination,
  updateResult,
  className,
}) => {
  return (
    <>
      <div id="sidebar" className={`${className} col-lg-2`}>
        <p
          id="filter-title"
          className="font-weight-bold mt-3 text-center text-lg-left"
        >
          Filters
        </p>
        <div
          id="radio-button-group"
          className="d-flex d-lg-block justify-content-around"
        >
          <div className="d-block d-md-flex d-lg-block">
            <p className="font-weight-bold mr-md-2">Characters:</p>
            <div>
              <label className="mr-md-2">
                <input
                  className="radio mr-2"
                  name="filter"
                  type="radio"
                  value="character-name"
                  defaultChecked={true}
                  onClick={(e) => {
                    updateFilter(e.currentTarget.value);
                    updatePagination(1);
                    updateResult({});
                  }}
                />
                Name
              </label>
            </div>
            <div>
              <label>
                <input
                  className="radio mr-2"
                  name="filter"
                  type="radio"
                  value="character-type"
                  onClick={(e) => {
                    updateFilter(e.currentTarget.value);
                    updatePagination(1);
                    updateResult({});
                  }}
                />
                Type
              </label>
            </div>
          </div>
          <div className="d-block d-md-flex d-lg-block">
            <p className="font-weight-bold mr-md-2">Location:</p>
            <div>
              <label className="mr-md-2">
                <input
                  className="radio mr-2"
                  name="filter"
                  type="radio"
                  value="location-name"
                  onClick={(e) => {
                    updateFilter(e.currentTarget.value);
                    updatePagination(1);
                    updateResult({});
                  }}
                />
                Name
              </label>
            </div>
            <div>
              <label>
                <input
                  className="radio mr-2"
                  name="filter"
                  type="radio"
                  value="location-type"
                  onClick={(e) => {
                    updateFilter(e.currentTarget.value);
                    updatePagination(1);
                    updateResult({});
                  }}
                />
                Type
              </label>
            </div>
          </div>
          <div className="d-block d-md-flex d-lg-block">
            <p className="font-weight-bold mr-md-2">Episodes:</p>
            <div>
              <label className="mr-md-2">
                <input
                  className="radio mr-2"
                  name="filter"
                  type="radio"
                  value="episode-name"
                  onClick={(e) => {
                    updateFilter(e.currentTarget.value);
                    updatePagination(1);
                    updateResult({});
                  }}
                />
                Name
              </label>
            </div>
            <div>
              <label>
                <input
                  className="radio mr-2"
                  name="filter"
                  type="radio"
                  value="episode-episode"
                  onClick={(e) => {
                    updateFilter(e.currentTarget.value);
                    updatePagination(1);
                    updateResult({});
                  }}
                />
                Episode
              </label>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default connect(null, { updateFilter, updatePagination, updateResult })(
  Sidebar
);
