import moment from "moment";
import { MDBIcon } from "mdbreact";

const Footer = () => {
  const fecha = moment().format("D/M/YYYY");

  return (
    <>
      <div
        id="footer"
        className=" d-flex justify-content-between font-weight-bold text-uppercase"
      >
        <div className="ml-2 mt-3">Tania Ibarra</div>
        <MDBIcon
          onClick={(e) => {
            e.preventDefault();
            window.location.href = "https://www.linkedin.com/in/tani-ibarra/";
          }}
          fab
          icon="linkedin"
          size="2x"
          className="icon my-2"
        />
        <MDBIcon
          onClick={(e) => {
            e.preventDefault();
            window.location.href = "https://gitlab.com/taniibarra";
          }}
          fab
          icon="github"
          size="2x"
          className="icon my-2"
        />
        <div className="mr-2 mt-3">{fecha}</div>
      </div>
    </>
  );
};

export default Footer;
