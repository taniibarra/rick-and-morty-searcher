import updateSearchText from "../../store/search-text/search-text.action";
import updateResult from "../../store/result/result.action";
import updatePagination from "../../store/pagination/pagination.action";
import { connect } from "react-redux";
import { useRef } from "react";

interface SearcherProps {
  updateSearchText?: any;
  updateResult?: any;
  updatePagination?: any;
}

const Searcher: React.FC<SearcherProps> = ({
  updateSearchText,
  updateResult,
  updatePagination,
}) => {
  const searchTextRef: React.MutableRefObject<any> = useRef(null);

  const keyEvents = (e: React.KeyboardEvent<HTMLInputElement>): void => {
    const text = e.currentTarget.value.trim();
    if (text.length > 2) {
      updateSearchText(text);
      updatePagination(1);
    } else {
      updateSearchText("");
      updatePagination(1);
      updateResult({});
    }
  };

  return (
    <>
      <div
        id="searcher"
        className="d-sm-flex align-items-center justify-content-end"
      >
        <div className="col-sm-5 mt-2 mt-md-0">
          <input
            className="form-control"
            type="text"
            placeholder="Search"
            aria-label="Search"
            ref={searchTextRef}
            onKeyUp={(e) => {
              keyEvents(e);
            }}
          />
        </div>
        <div className="mr-3 d-flex">
          <button
            id="clear-button"
            className="btn px-4 py-2 font-weight-bold mx-auto"
            onClick={() => {
              searchTextRef.current.value = "";
              updateSearchText("");
              updateResult({});
            }}
          >
            RESET ALL
          </button>
        </div>
      </div>
    </>
  );
};

export default connect(null, {
  updateSearchText,
  updateResult,
  updatePagination,
})(Searcher);
