import Footer from "./footer.component";
import Sidebar from "./sidebar.component";
import Searcher from "./searcher.component";

interface LayoutProps {
  children?: any;
}
const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <>
      <div id="layout" className="d-flex flex-column position-absolute">
        <Searcher />
        <div
          id="central-section"
          className="d-flex flex-column flex-lg-row flex-grow-1"
        >
          <Sidebar />
          <div id="content" className="p-3 grey lighten-3 flex-grow-1">
            {children}
          </div>
        </div>
        <Footer />
      </div>
    </>
  );
};

export default Layout;
