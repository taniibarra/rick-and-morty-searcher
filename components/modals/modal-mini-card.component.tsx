const MiniCard = ({ name, image }) => {
  return (
    <>
      <div className="col-3 mini-card m-2">
        <div>
          <img className="img-fluid" src={image} alt={name} />
        </div>
        <p className="text-center font-weight-bold mt-3">{name}</p>
      </div>
    </>
  );
};

export default MiniCard;
