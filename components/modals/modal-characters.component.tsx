import { MDBModalBody, MDBModalHeader } from "mdbreact";
import { useState } from "react";

const ModalCharacters = ({ data }) => {
  const [open, setOpen] = useState(true);
  const toggle = () => {
    setOpen(!open);
  };

  const { MDBModal } = require("mdbreact");

  return (
    <>
      <MDBModal isOpen={open} toggle={toggle}>
        <MDBModalHeader toggle={toggle}></MDBModalHeader>
        <MDBModalBody className="text-center">
          <div className="my-2">
            <img className="img-fluid" src={data.image} alt={data.name} />
          </div>
          <p className="text-center font-weight-bold mt-4">{data.name}</p>
          <p className="font-weight-bold">
            Type:{" "}
            <small className="ml-1">{data.type === "" ? "-" : data.type}</small>
          </p>
          <p className="font-weight-bold">
            Genre:{" "}
            <small className="ml-1">
              {data.gender === "" ? "-" : data.gender}
            </small>
          </p>
          <p className="font-weight-bold">
            Species:{" "}
            <small className="ml-1">
              {data.species === "" ? "-" : data.species}
            </small>
          </p>
        </MDBModalBody>
      </MDBModal>
    </>
  );
};

export default ModalCharacters;
