import MiniCard from "./modal-mini-card.component";
import { v4 as uuidv4 } from "uuid";
import { MDBModalBody, MDBModalHeader } from "mdbreact";
import { useState } from "react";

const ModalLocEp = ({ data }) => {
  const [open, setOpen] = useState(true);

  const toggle = () => {
    setOpen(!open);
  };

  const arrayCharacters = data.residents || data.characters;

  let miniCards = [];
  if (arrayCharacters[0]) {
    for (let i = 0; i < arrayCharacters.length; i++) {
      const ch = arrayCharacters[i];
      if (ch.name !== null) {
        miniCards.push(ch);
      }
      if (i === 4) {
        break;
      }
    }
  }

  const { MDBModal } = require("mdbreact");

  return (
    <>
      <MDBModal isOpen={open} toggle={toggle}>
        <MDBModalHeader toggle={toggle}></MDBModalHeader>
        <MDBModalBody className="text-center">
          <p className="text-center font-weight-bold mt-4">{data.name}</p>

          {data.type ? (
            <p className="font-weight-bold">
              Type: <small className="ml-1">{data.type}</small>
            </p>
          ) : null}
          {data.air_date ? (
            <p className="font-weight-bold">
              Release Date: <small className="ml-1">{data.air_date}</small>
            </p>
          ) : null}

          {data.dimension ? (
            <p className="font-weight-bold">
              Dimension: <small className="ml-1">{data.dimension}</small>
            </p>
          ) : null}
          {data.episode ? (
            <p className="font-weight-bold">
              Episode: <small className="ml-1">{data.episode}</small>
            </p>
          ) : null}
          <div className="d-flex flex-wrap justify-content-center">
            {miniCards[0] ? (
              miniCards.map((ch) => (
                <MiniCard key={uuidv4()} name={ch.name} image={ch.image} />
              ))
            ) : (
              <p className="font-weight-bold">No characters found!</p>
            )}
          </div>
        </MDBModalBody>
      </MDBModal>
    </>
  );
};

export default ModalLocEp;
