import { MDBContainer, MDBAlert } from "mdbreact";

const NotFound = () => {
  return (
    <>
      <MDBContainer>
        <MDBAlert color="warning" className="text-center">
          No Results!
        </MDBAlert>
      </MDBContainer>
    </>
  );
};

export default NotFound;
