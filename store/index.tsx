import { createStore, combineReducers } from "redux";
import filterReducer from "./filter/filter.reducer";
import searchTextReducer from "./search-text/search-text.reducer";
import resultReducer from "./result/result.reducer";
import paginationReducer from "./pagination/pagination.reducer";
import loadingReducer from "./loading/loading.reducer";
import { qry } from "../services/apollo.service";

const reducers = combineReducers({
  filterReducer,
  searchTextReducer,
  resultReducer,
  paginationReducer,
  loadingReducer,
});

const store = createStore(reducers);

let oldSearch = "";
store.subscribe(async () => {
  const filter = store.getState().filterReducer.filter;
  const searchText = store.getState().searchTextReducer.searchText;
  const pagination = store.getState().paginationReducer.pagination;

  if (searchText !== "" && oldSearch !== filter + searchText + pagination) {
    oldSearch = filter + searchText + pagination;
    let startQuery: any;
    let endQuery = `
      }){
        info {
          count
          pages
        }
        results {
          name`;

    if (filter.includes("character")) {
      startQuery = `characters (page:${pagination} filter: {`;
      endQuery += `
          image
          gender
          species
          type 
        }
      }`;
    } else if (filter.includes("location")) {
      startQuery = `locations (page:${pagination} filter: {`;
      endQuery += `
          dimension
          type
          residents {
            name
            image
          }
        }
      }`;
    } else {
      startQuery = `episodes (page:${pagination} filter: {`;
      endQuery += `
          episode
          air_date
          characters {
            name
            image
          }
        }
      }`;
    }

    let filterQuery: string;

    if (filter.includes("name")) {
      filterQuery = " name: ";
    } else if (filter.includes("type")) {
      filterQuery = " type: ";
    } else {
      filterQuery = " episode: ";
    }

    const query = `
    ${startQuery}
    ${filterQuery} "${searchText}"
    ${endQuery}
  `;
    try {
      store.dispatch({
        type: "UPDATE_LOADING",
        payload: true,
      });

      const data = await qry(query);
      store.dispatch({
        type: "UPDATE_LOADING",
        payload: false,
      });

      store.dispatch({
        type: "UPDATE_RESULT",
        payload: data.data,
      });
    } catch (error) {
      store.dispatch({
        type: "UPDATE_LOADING",
        payload: false,
      });

      if (String(error).includes("404")) {
        store.dispatch({
          type: "UPDATE_RESULT",
          payload: { notFound: true },
        });
      } else {
        store.dispatch({
          type: "UPDATE_RESULT",
          payload: { error: true },
        });
      }
    }
  }
});

export default store;
