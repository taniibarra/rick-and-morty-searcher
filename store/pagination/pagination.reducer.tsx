import { Reducer } from "redux";

const initialState = {
  pagination: 1,
};

const paginationReducer: Reducer = (state = initialState, action) => {
  if (action.type === "UPDATE_PAGINATION") {
    return {
      ...state,
      pagination: action.payload,
    };
  }

  return state;
};

export default paginationReducer;
