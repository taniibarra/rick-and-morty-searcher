const updatePagination = (pagination: number) => {
  return {
    type: "UPDATE_PAGINATION",
    payload: pagination,
  };
};

export default updatePagination;
