const updateResult = (result: object) => {
  return {
    type: "UPDATE_RESULT",
    payload: result,
  };
};

export default updateResult;
