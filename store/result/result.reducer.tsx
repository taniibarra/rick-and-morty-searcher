import { Reducer } from "redux";

const initialState = {
  result: {},
};

const resultReducer: Reducer = (state = initialState, action) => {
  if (action.type === "UPDATE_RESULT") {
    return {
      ...state,
      result: action.payload,
    };
  }

  return state;
};

export default resultReducer;
