import { Reducer } from "redux";

const initialState = {
  loading: false,
};

const loadingReducer: Reducer = (state = initialState, action) => {
  if (action.type === "UPDATE_LOADING") {
    return {
      ...state,
      loading: action.payload,
    };
  }

  return state;
};

export default loadingReducer;
