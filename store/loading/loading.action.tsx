const upadteLoading = (loading: boolean) => {
  return {
    type: "UPDATE_LOADING",
    payload: loading,
  };
};

export default upadteLoading;
