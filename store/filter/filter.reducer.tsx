import { Reducer } from "redux";

const initialState = {
  filter: "character-name",
};

const filterReducer: Reducer = (state = initialState, action) => {
  if (action.type === "UPDATE_FILTER") {
    return {
      ...state,
      filter: action.payload,
    };
  }

  return state;
};

export default filterReducer;
