const updateFilter = (filter: string) => {
  return {
    type: "UPDATE_FILTER",
    payload: filter,
  };
};

export default updateFilter;
