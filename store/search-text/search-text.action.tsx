const updateSearchText = (searchText: string) => {
  return {
    type: "UPDATE_SEARCHTEXT",
    payload: searchText,
  };
};

export default updateSearchText;
