import { Reducer } from "redux";

const initialState = {
  searchText: "",
};

const searchTextReducer: Reducer = (state = initialState, action) => {
  if (action.type === "UPDATE_SEARCHTEXT") {
    return {
      ...state,
      searchText: action.payload,
    };
  }

  return state;
};

export default searchTextReducer;
