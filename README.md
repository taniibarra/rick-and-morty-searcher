## **Rick and Morty Searcher**

Search for any information about the Rick & Morty program through the three filters provided. One for the characters, one for the locations and one for the episodes. The search can be done both by type or name.

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Let's code!

**Clone this repository** *Make sure you have npm and node installed*
Go to the project directory with the "cd rick-and-morty-searcher" command and type:

- npm install

**For development**
- npm run dev

**For production**

- next build
- next start

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

**You should see the next page:**

![Rick and Morty Page](assets/rick-and-morty.png)

**And you can try the app through the following link:**

[Demo Rick and Morty App](https://rick-and-morty-searcher.vercel.app/)

